from django.http import Http404, HttpResponseRedirect

from django.urls import reverse

from django.shortcuts import render

from datetime import datetime

from .models import Article, Comment


def index(request):
    latest_articles_list = Article.objects.order_by('-pub_date')[:5]
    return render(request, 'index.html', {'latest_articles_list': latest_articles_list})


def detail(request, article_id):
    try:
        a = Article.objects.get(id=article_id)
    except:
        raise Http404("Статья не найдена!")
    latest_comment_list = a.comment_set.order_by('-id')[:10]
    return render(request, 'project/detail.html', {'article': a, 'latest_comment_list': latest_comment_list})


def comments(request):
    comment = Comment.objects.all()
    return render(request, 'project/list.html', {'comments': comment})


def articles(request):
    article = Article.objects.all()
    return render(request, 'project/answer_list.html', {'articles': article})


def menu(request):
    return render(request, 'project/menu.html')
    
def leave_quest(request):
    return render(request, 'project/addanswer.html')

def authors(request):
    return render(request, 'project/authors.html')

def leave_comment(request, article_id):
    try:
        a = Article.objects.get(id=article_id)
    except:
        raise Http404("Статья не найдена!")
    a.comment_set.create(author_name=request.POST['name'], comment_text=request.POST['text'], comment_date=datetime.now())
    return HttpResponseRedirect(reverse('articles:detail', args=(a.id,)))
    
def addart(request):
    a = Article.objects.create(article_author=request.POST['author'], article_title=request.POST['title'], article_text=request.POST['text'], article_photo=request.FILES['photo'], pub_date=datetime.now())
    return HttpResponseRedirect(reverse('articles:detail', args=(a.id,)))
