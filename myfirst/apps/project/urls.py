from django.urls import path
from . import views

app_name = 'articles'
urlpatterns = [
    path('', views.index, name='index'),
    path('<int:article_id>/', views.detail, name='detail'),
    path('comments/', views.comments, name='comments'),
    path('answer_list/', views.articles, name='answer_list'),
    path('menu/', views.menu, name='menu'),
    path('authors/', views.authors, name='authors'),
    path('articles/add', views.leave_quest, name='leave_quest'),
    path('articles/new', views.addart, name='addart'),
    path('<int:article_id>/leave_comment/', views.leave_comment, name='leave_comment'),
]