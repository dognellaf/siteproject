
from django.db import models

from django.utils import timezone


class Article(models.Model):
    article_author = models.CharField('Автор', max_length=200)
    article_photo = models.ImageField('Изображение')
    article_title = models.CharField('Название вопроса', max_length=200)
    article_text = models.TextField('Текст вопроса', max_length=15000)
    pub_date = models.DateTimeField('Дата публикации')
    
    def __str__(self):
        return self.article_title

    class Meta:
        verbose_name = 'Вопрос'
        verbose_name_plural = 'Вопрос'


class Comment(models.Model):
    article = models.ForeignKey(Article, on_delete=models.CASCADE)
    author_name = models.CharField('Имя автора', max_length=75)
    comment_text = models.CharField('Текст комментария', max_length=500)
    comment_date = models.DateTimeField('Дата ответа')

    def __str__(self):
        return self.author_name

    class Meta:
        verbose_name = 'Комментарий'
        verbose_name_plural = 'Комментарии'
